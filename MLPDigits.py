"""
==========================================================================================
Neural Network based on MLPClassifier for Digit Recognition and Hidden Layer Visualization
==========================================================================================

Author: Haotian Shi, 2017

"""
#print(__doc__)

import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import GridSearchCV
from PIL import Image

# generate data
X = np.loadtxt("data/digitsX.dat", delimiter=',')
y = np.loadtxt("data/digitsY.dat", delimiter=',')

# optimize Neural Network for Digit Recognition
MLP_clf = MLPClassifier(solver='adam', hidden_layer_sizes=(25,), max_iter=600, random_state=19)
#parameters = {'activation':('identity', 'logistic', 'tanh', 'relu'), 'learning_rate_init':(0.005, 0.01, 0.015, 0.017, 0.019, 0.02, 0.022, 0.024, 0.025, 0.03, 0.04, 0.05, 0.1), 'alpha':(0.001, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.1)}
parameters = {'activation':('logistic',), 'learning_rate_init':( 0.01, 0.016, 0.0168, 0.017, 0.02), 'alpha':(0.05, 0.06, 0.068, 0.07)}
MLP_gs_clf = GridSearchCV(MLP_clf, parameters, cv=3)
MLP_gs_clf.fit(X, y)
print 'Optimal Parameters: ',MLP_gs_clf.best_params_
print 'Optimal Training Performance: ',MLP_gs_clf.best_score_

# Visualizing Hidden Layers using PIL
best = MLP_gs_clf.best_params_
MLP_optimal_clf = MLPClassifier(solver='adam', hidden_layer_sizes=(25,), max_iter=400, random_state=19, activation=best['activation'], learning_rate_init=best['learning_rate_init'], alpha=best['alpha'])
MLP_optimal_clf.fit(X, y)
fig, axes = plt.subplots(5, 5)
vmin, vmax = MLP_optimal_clf.coefs_[0].min(), MLP_optimal_clf.coefs_[0].max()
for coef, ax in zip(MLP_optimal_clf.coefs_[0].T, axes.ravel()):
    ax.matshow(coef.reshape(20, 20), cmap=plt.cm.gray, vmin=0.5 * vmin,vmax=0.5 * vmax)
    ax.set_xticks(())
    ax.set_yticks(())
    
filename = 'visualizeHiddenNodesDigits'
fig.savefig(filename + '.png')
img = Image.open(filename + '.png')
img.save(filename + '.ppm')
os.remove(filename + '.png')
